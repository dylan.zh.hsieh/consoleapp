﻿using System;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var counter = 0;
            var max = args.Length != 0 ? Convert.ToInt32(args[0]) : -1;
            var fabUrl = Environment.GetEnvironmentVariable("FabUrl");
            var who = Environment.GetEnvironmentVariable("who");
 
            Console.WriteLine("Environment-fabUrl: " + fabUrl);
            Console.WriteLine("Environment-who: " + who);
 
            while (max == -1 || counter < max)
            {
                Console.WriteLine($"Counter: {++counter}");
                Console.WriteLine("Environment-fabUrl: " + fabUrl);
                Console.WriteLine("Environment-who: " + who);

                await Task.Delay(1000);
            }
        }
    }
}
